package com.xy.solution;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocket serversocket = new ServerSocket(10002);
        Socket accept = null;
        BufferedReader br=null ;
        PrintWriter pw =null;
        while (true) {
            //TODO : 每次accept到一个连接之后都要放入新开的线程
            accept=serversocket.accept();
            System.out.println("接收到客户端请求");
            new ServerService(accept).start();
        }
    }
}
