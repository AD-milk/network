package com.xy.solution;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerService extends Thread {
    private Socket socket;

    public ServerService(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {//TODO : 新开线程需要判断，当客户端关闭时释放连接。可以根据接收到的字节数为0来判断
        BufferedReader br=null ;
        PrintWriter pw =null;
        try {
            while (true) {
                //TODO : 每次accept到一个连接之后都要放入新开的线程
                br= new BufferedReader(new InputStreamReader(socket.getInputStream()));
                pw=new  PrintWriter(socket.getOutputStream());
                String message = br.readLine();
                if(br==null||br.equals("quit")){
                    System.out.println("客户端下线");
                    socket.close();
                    return;
                }
                System.out.println("message from client:"+message);
                if (message.equals("TEXT")) {
                    new CharThread(socket).start();
                }else if(message.equals("BINARY")){
                    new ByteThread(socket).start();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                System.out.println("客户端断开连接断开");
                br.close();
                pw.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
