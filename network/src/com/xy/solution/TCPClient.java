package com.xy.solution;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class TCPClient {
    public static void main(String[] args) throws IOException {
        Socket socket  = new Socket("127.0.0.1", 10002);
        PrintWriter pw = null;
        BufferedReader br =null;
        String cmd=null;
        int i=0;
        while (true) {
            i++;
            pw = new PrintWriter(socket.getOutputStream());
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            Scanner scanner = new Scanner(System.in);
            cmd = scanner.nextLine();
            pw.write(cmd + "\r\n");
            pw.flush();
            if(cmd.equals("quit")){
                socket.close();
                return;
            }
            BufferedReader finalBr = br;
            int finalI = i;
            new Thread(()->{
                try {
                    FileWriter fileWriter = new FileWriter("D:\\AText\\reChar" + finalI + ".txt");
                    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                    String line = null;
                    while ((line = finalBr.readLine()) != null) {
                        bufferedWriter.write(line);
                    }
                    bufferedWriter.close();
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
