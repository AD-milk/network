package com.xy.solution;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class ByteThread extends Thread{
    private Socket socket;

    public ByteThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            int len;
            FileInputStream fis = new FileInputStream(new File("D:\\AText\\textFile.txt"));
            OutputStream os = socket.getOutputStream();

            byte[] buf=new byte[1024];
            while ((len=fis.read(buf))!=-1){
                os.write(buf,0,len);
            }
            fis.close();
            // TODO: 这里的shutdown应该不需要
            socket.shutdownOutput();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
