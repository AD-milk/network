package com.xy.solution;



import java.io.*;
import java.net.Socket;

public class CharThread extends Thread{
    private Socket socket;


    public CharThread(Socket s) throws IOException {
        this.socket=s;
    }

    @Override
    public void run() {
        PrintWriter pw=null;
        FileReader fr=null;
        BufferedReader br=null;

        try {
            pw=new PrintWriter(socket.getOutputStream());
            fr=new FileReader("D:\\AText\\textFile.txt");
            br=new BufferedReader(fr);
            String line=null;
            while ((line=br.readLine())!=null){
                pw.write(line);
                pw.flush();
            }
            br.close();
            fr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                br.close();
                fr.close();
                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}
